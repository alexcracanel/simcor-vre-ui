import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VirtualCohortGeneratorComponent } from './virtual-cohort-generator/virtual-cohort-generator.component';
import { ApplicationsComponent } from './applications/applications.component';

const routes: Routes = [
  { path: '', component: ApplicationsComponent },
  { path: 'generator', component: VirtualCohortGeneratorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
