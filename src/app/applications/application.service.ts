import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { APPLICATIONS } from './application-registry';
import { Application } from './application';


@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor() { }

  getApplications(): Observable<Application[]> {
    const applications = of(APPLICATIONS);
    return applications;
  }
}
