import { Application } from './application';

//export const APPLICATIONS: Application[] = [
//  { id: 1, title: 'Virtual Cohort Generator', subtitle: 'Virtual Cohort Generator', link: 'generator', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Blandit libero volutpat sed cras ornare arcu. Sagittis vitae et leo duis ut diam.' }
//];

export const APPLICATIONS: Application[] = [
  { id: 1, title: 'Virtual Cohort Generator', subtitle: 'Virtual Cohort Generator', link: 'generator', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Blandit libero volutpat sed cras ornare arcu. Sagittis vitae et leo duis ut diam.' },
  { id: 2, title: 'Statistical environment', subtitle: 'Statistical environment for in-silico trials', link: 'https://simcor.unitbv.ro', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Blandit libero volutpat sed cras ornare arcu. Sagittis vitae et leo duis ut diam.' }
];