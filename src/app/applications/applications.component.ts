import { Component, OnInit, Input } from '@angular/core';

import { Application } from './application';
import { ApplicationService } from './application.service';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit {

  applications: Application[] = [];

  public grid_cols?: number;

  constructor(private applicationService: ApplicationService) { }

  ngOnInit(): void {
    this.setGridColumns(window.innerWidth);
    this.getApplications();
  }

  getApplications(): void {
    this.applicationService.getApplications()
        .subscribe(applications => this.applications = applications);
  }

  setGridColumns(sizeOfWindow: number) {
    if (sizeOfWindow > 1600) {
      this.grid_cols = 4;
    }
    if (sizeOfWindow <= 1600) {
      this.grid_cols = 3;
    }
    if (sizeOfWindow <= 1400) {
      this.grid_cols = 2;
    }
    if (sizeOfWindow <= 730) {
      this.grid_cols = 1;
    }
  }

  onResize(event: any) {
    this.setGridColumns(event.target.innerWidth);
  }

}

