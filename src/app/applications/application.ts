export interface Application {
  id: number;
  title: string;
  subtitle: string;
  description: string;
  link: string;
}