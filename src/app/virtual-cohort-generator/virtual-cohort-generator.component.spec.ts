import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualCohortGeneratorComponent } from './virtual-cohort-generator.component';

describe('VirtualCohortGeneratorComponent', () => {
  let component: VirtualCohortGeneratorComponent;
  let fixture: ComponentFixture<VirtualCohortGeneratorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VirtualCohortGeneratorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualCohortGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
